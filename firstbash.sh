#!/bin/bash
#

count=0

# bash trap command
trap bashtrap INT
# bash clear screen command
clear;
# bash trap function is executed when CTRL-C is pressed:
# bash prints message => Executing bash trap subrutine !
bashtrap()
{
        echo "CTRL+C Detected !...executing bash trap !"
	#exit
}

echo `uname -a`
while [ $count -lt 100 ]; do
	let count=count+1
	echo "$count: this is my first bash script running"
	sleep 5
done

